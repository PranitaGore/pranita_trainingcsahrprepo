﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_SumOfDigits
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter two digit number: ");
            int number =int.Parse( Console.ReadLine());
            if(number>=10 && number<=99)
            {
                int sum= AddDigits(number);
                int sub = number - sum; 
                int result = AddDigits(sub);
                Console.WriteLine("Final addition is: "+result);
            }
            else
            {
                Console.WriteLine("Number is not 2 digits..");
            }
            Console.ReadLine();

        }

        private static int AddDigits(int num)
        {
            int remender = 0; int sum = 0;
            while (num != 0)
            {
                remender = num % 10;
                num = num / 10;
                sum = sum + remender;
            }
            return sum;
        }
    }
}
