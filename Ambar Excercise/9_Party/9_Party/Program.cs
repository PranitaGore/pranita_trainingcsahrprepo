﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9_Party
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter total no of people in party: ");
            int people = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter total size of group: ");
            int groupSize = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter cost of Token: ");
            int tokenACost = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter cost of Gifts: ");
            int costGift = int.Parse(Console.ReadLine());
            int totalCoast=CalculateCost(people, groupSize, tokenACost, costGift);
            Console.WriteLine("Total Cost for Training: "+totalCoast);
            Console.ReadLine();
        }

        private static int CalculateCost(int people, int groupSize, int tokenACost, int costGift)
        { 
            int noOfGroups = people / groupSize;
            int remainingPeople = people % groupSize;
            int tokenGivenPeople = people - remainingPeople;
            return  (tokenGivenPeople * tokenACost) + (remainingPeople * costGift);
        }
    }
}
