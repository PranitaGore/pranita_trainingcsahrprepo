﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_vowels
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter string : ");
            string inputString = Console.ReadLine();
            if(inputString!=string.Empty)
            {
                string[] arr = TokenizeString(inputString);
                if(arr.Count()>0)
                {
                    string output = string.Empty;
                    foreach (string str in arr)
                    {
                        output += str + ",";
                    }
                    Console.WriteLine("output is: " + output.Remove(output.Length-1));
                }
                else
                    Console.WriteLine("No any delemeter present in string.."+arr[0]);
                
            }
            else
                Console.WriteLine("No string entered..");
            
            Console.ReadLine();
        }

        private static string[] TokenizeString(string inputString)
        {
            //inputString = inputString.ToLower();
            //string[] arr = inputString.Split(new[] {'a', 'e', 'i', 'o', 'u' },StringSplitOptions.None);
            string[] arr = inputString.Split(new[] {'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U' },StringSplitOptions.RemoveEmptyEntries);
            return arr;
        }
    }
}
