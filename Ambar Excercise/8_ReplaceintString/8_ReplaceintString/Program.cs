﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8_ReplaceintString
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter first string..");
            string str1 = Console.ReadLine();
            if(str1 != string.Empty)
            {
                char[] arr1 = str1.ToCharArray();
                string res1 = string.Empty;
                foreach (char c in arr1)
                {
                    if (c == 'A')
                    {
                        res1 += ',';
                    }
                    else if (c == 'B')
                    {
                        res1 += ';';
                    }
                    else
                        res1 += c;
                }
                Console.WriteLine("Replaced string: " + res1);
            }
            else
                Console.WriteLine("String not entered...");

            Console.WriteLine("Enter second string..");
            string str2 = Console.ReadLine();
            if(str2!=string.Empty)
            {
                char[] arr2 = str2.ToCharArray();
                string res2 = string.Empty;
                foreach (char ch in arr2)
                {
                    if (ch == 'Q')
                    {
                        res2 += '_';
                    }
                    else if (ch == 'R')
                    {
                        res2 += '#';
                    }
                    else
                        res2 += ch;
                }
                Console.WriteLine("Replaced string is: " + res2);
            }
            else
                Console.WriteLine("String not entered...");

            Console.ReadLine();
        }
    }
}
