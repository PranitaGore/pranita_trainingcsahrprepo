﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5_DateValidator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter date: ");
            string dt = Console.ReadLine();
            bool bl;
            DateTime d;
            bl = DateTime.TryParseExact(dt, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out d);
            if(!bl)
              Console.WriteLine("Entered date is not in valid format..");
            else
                Console.WriteLine("Date is valid..");
            Console.ReadLine();
        }
    }
}
