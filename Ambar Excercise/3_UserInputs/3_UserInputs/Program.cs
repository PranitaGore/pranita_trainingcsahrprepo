﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_UserInputs
{
    class Program
    {
        static void Main(string[] args)
        {
            string userInput;
            Console.WriteLine("Enter string : ");
            userInput= Console.ReadLine();
            if(userInput!=string.Empty)
            {
                Console.WriteLine("1.Convert the string to all capitals");
                Console.WriteLine("2.Convert the string to all small letters");
                Console.WriteLine("3.Add spaces after each letter");
                Console.WriteLine("4.Remove spaces");
                OperationsOnString(userInput);
            }
            else
                Console.WriteLine("String is not entered..");
            Console.ReadLine();
        }

        private static void OperationsOnString(string userInput)
        {
            do
            {
                int num = int.Parse(Console.ReadLine());
                switch (num)
                {
                    case 1:
                        Console.WriteLine(userInput.ToUpper());
                        break;
                    case 2:
                        Console.WriteLine(userInput.ToLower());
                        break;
                    case 3:
                        string removedSpaces = userInput.Replace(" ", "");
                        Console.WriteLine(String.Join<char>(",", removedSpaces));
                        break;
                    case 4:
                        Console.WriteLine(userInput.Replace(" ", ""));
                        break;
                    default:
                        Console.WriteLine("Wrong input....");
                        break;
                }
                Console.WriteLine("Do you want to check another function ? y/n ");
            } while (Console.ReadLine().ToUpper() == "Y");
        }
    }
}
