﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10_Shirts
{
    class Program
    {
        static bool flag = false;
        static void Main(string[] args)
        {
            double totalLength = 6;
            Console.WriteLine("Enter types of shirt: ");
            int types = int.Parse(Console.ReadLine());
            double lengthPerType = totalLength / types;
            double result = GetShirtInfo(types,lengthPerType);        
            if(!flag)
            {
                Console.WriteLine("Total length remaining is: " + (totalLength - result));
            }
            Console.ReadLine();
        }

        private static double GetShirtInfo(int types,double lengthPerType)
        {
            double result = 0;
            try
            {
                for (int i = 0; i < types; i++)
                {
                    string[] arr = Console.ReadLine().Split(' ');
                    if (double.Parse(arr[0]) <= lengthPerType)
                    {
                        result += CalLength(arr);
                    }
                    else
                    {
                        Console.WriteLine("length of cloth for shirt is greater than legnth given to per shirt..");
                        flag = true;
                        break;
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Error occured.."+e.Message);
            }
            return result;
        }

        private static double CalLength(string[] arr)
        {
            return double.Parse(arr[0]) * double.Parse(arr[1]);
        }
    }
}
